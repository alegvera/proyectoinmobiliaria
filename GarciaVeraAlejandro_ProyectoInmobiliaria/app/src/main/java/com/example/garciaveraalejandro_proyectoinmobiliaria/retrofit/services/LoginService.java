package com.example.garciaveraalejandro_proyectoinmobiliaria.retrofit.services;

import com.example.garciaveraalejandro_proyectoinmobiliaria.responses.LoginResponse;
import com.example.garciaveraalejandro_proyectoinmobiliaria.model.Registro;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface LoginService {

    @POST("/auth")
    Call<LoginResponse> doLogin();

    @POST("/users")
    Call<LoginResponse> doRegister(@Body Registro registro);
}