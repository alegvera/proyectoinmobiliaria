package com.example.garciaveraalejandro_proyectoinmobiliaria;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.garciaveraalejandro_proyectoinmobiliaria.responses.LoginResponse;
import com.example.garciaveraalejandro_proyectoinmobiliaria.model.Registro;
import com.example.garciaveraalejandro_proyectoinmobiliaria.retrofit.UtilToken;
import com.example.garciaveraalejandro_proyectoinmobiliaria.retrofit.generator.ServiceGenerator;
import com.example.garciaveraalejandro_proyectoinmobiliaria.retrofit.services.LoginService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistroUser extends AppCompatActivity {

    EditText txt_email, txt_name, txt_password, txt_camara;
    Button btn_registro;
    TextView tv_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registro);

        txt_email = findViewById(R.id.txt_email);
        txt_name = findViewById(R.id.txt_name);
        txt_password = findViewById(R.id.txt_password);
        btn_registro = findViewById(R.id.btn_registro);
        tv_login = findViewById(R.id.tv_login);

        btn_registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name = txt_name.getText().toString().trim();
                String email = txt_email.getText().toString().trim();
                String password = txt_password.getText().toString().trim();

                Registro registro = new Registro(email, name, password);


                LoginService service = ServiceGenerator.createService(LoginService.class);


                Call<LoginResponse> loginReponseCall = service.doRegister(registro);


                loginReponseCall.enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                        if (response.code() == 201) {
                            UtilToken.setToken(RegistroUser.this, response.body().getToken());
                            startActivity(new Intent(RegistroUser.this, PaginaPrincipal.class));

                        } else {
                            // error
                            Toast.makeText(RegistroUser.this, "Error en el registro. Revise los datos introducidos", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        Log.e("NetworkFailure", t.getMessage());
                        Toast.makeText(RegistroUser.this, "Error de conexión", Toast.LENGTH_SHORT).show();

                    }
                });
            }
        });

        tv_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(
                        RegistroUser.this,
                        LoginActivity.class
                );

                startActivity(i);

            }
        });

    }
}
