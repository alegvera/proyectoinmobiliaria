package com.example.garciaveraalejandro_proyectoinmobiliaria;

import com.example.garciaveraalejandro_proyectoinmobiliaria.model.Property;
import com.example.garciaveraalejandro_proyectoinmobiliaria.model.ResponseContainer;
import com.example.garciaveraalejandro_proyectoinmobiliaria.responses.PropertyResponse;

public interface PropertyInteractionListener {
    public void onPropertyExpandClick(PropertyResponse property);

    public void onClickDelete(String id);
}
