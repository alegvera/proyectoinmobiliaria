package com.example.garciaveraalejandro_proyectoinmobiliaria.retrofit.services;

import com.example.garciaveraalejandro_proyectoinmobiliaria.model.Property;
import com.example.garciaveraalejandro_proyectoinmobiliaria.model.ResponseContainer;
import com.example.garciaveraalejandro_proyectoinmobiliaria.responses.PropertyOneResponse;
import com.example.garciaveraalejandro_proyectoinmobiliaria.responses.PropertyResponse;
import com.example.garciaveraalejandro_proyectoinmobiliaria.responses.ResponseContainerOneRow;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface PropertyService {
    @GET("/properties")
    Call<ResponseContainer<PropertyResponse>> listProperty();

    @DELETE("/properties/{id}")
    Call<ResponseBody> delete(@Path("id") String id);

    @GET("/properties/{id}")
    Call<ResponseContainerOneRow<PropertyOneResponse>> getOne(@Path("id") String id);
}
