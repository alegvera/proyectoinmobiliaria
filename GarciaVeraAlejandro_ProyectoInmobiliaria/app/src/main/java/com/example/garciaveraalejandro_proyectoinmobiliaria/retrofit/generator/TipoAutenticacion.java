package com.example.garciaveraalejandro_proyectoinmobiliaria.retrofit.generator;

public enum TipoAutenticacion {
    SIN_AUTENTICACION, BASIC, JWT
}
