package com.example.garciaveraalejandro_proyectoinmobiliaria;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.garciaveraalejandro_proyectoinmobiliaria.model.Category;
import com.example.garciaveraalejandro_proyectoinmobiliaria.model.ResponseContainer;
import com.example.garciaveraalejandro_proyectoinmobiliaria.model.User;
import com.example.garciaveraalejandro_proyectoinmobiliaria.responses.PropertyOneResponse;
import com.example.garciaveraalejandro_proyectoinmobiliaria.responses.PropertyResponse;
import com.example.garciaveraalejandro_proyectoinmobiliaria.responses.ResponseContainerOneRow;
import com.example.garciaveraalejandro_proyectoinmobiliaria.retrofit.UtilToken;
import com.example.garciaveraalejandro_proyectoinmobiliaria.retrofit.generator.ServiceGenerator;
import com.example.garciaveraalejandro_proyectoinmobiliaria.retrofit.generator.TipoAutenticacion;
import com.example.garciaveraalejandro_proyectoinmobiliaria.retrofit.services.PropertyService;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ImagenActivity extends AppCompatActivity {

    private String idObtenido;
    private FloatingActionButton delete;
    private ImageView imageViewUrlExpand;
    private TextView tvTitle;
    private TextView user;
    private TextView description;
    private TextView price;
    private TextView rooms;
    private TextView size;
    private TextView categoryId;
    private TextView address;
    private TextView zipcode;
    private TextView city;
    private TextView province;
    private TextView loc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imagen);
        Intent i = getIntent();
        idObtenido = i.getStringExtra("PROPERTY_ID");
        ImageView image = findViewById(R.id.imageViewUrlExpand);
        Bundle extras = getIntent().getExtras();
        Glide
                .with(this)
                .load(extras.getString("imageUrl"))
                .into(image);



        tvTitle=findViewById(R.id.NombreExpand);
        description = findViewById(R.id.description);
        user = findViewById(R.id.user);
        price = findViewById(R.id.price);
        rooms = findViewById(R.id.rooms);
        size = findViewById(R.id.size);
        categoryId = findViewById(R.id.categoryId);
        address = findViewById(R.id.address);
        zipcode = findViewById(R.id.zipcode);
        city = findViewById(R.id.city);
        province = findViewById(R.id.province);
        loc = findViewById(R.id.loc);


        PropertyService service = ServiceGenerator.createService(PropertyService.class);

        Call<ResponseContainerOneRow<PropertyOneResponse>> call = service.getOne(idObtenido);
        call.enqueue(new Callback<ResponseContainerOneRow<PropertyOneResponse>>() {
            @Override
            public void onResponse(Call<ResponseContainerOneRow<PropertyOneResponse>> call, Response<ResponseContainerOneRow<PropertyOneResponse>> response) {

                if (response.isSuccessful()){
                    PropertyOneResponse p = response.body().getRows();


                    //setear los campos
                    tvTitle.setText(p.getTitle());
                    description.setText(p.getDescription());
                    user.setText(String.valueOf(p.getOwnerId().getName()));
                    price.setText(String.valueOf(p.getPrice()));
                    rooms.setText(String.valueOf(p.getRooms()));
                    size.setText(String.valueOf(p.getSize()));
                    categoryId.setText(String.valueOf(p.getCategoryId().getName()));
                    address.setText(p.getAddress());
                    zipcode.setText(p.getZipcode());
                    city.setText(p.getCity());
                    province.setText(p.getProvince());
                    loc.setText(p.getLoc());





                }else{

                }
            }

            @Override
            public void onFailure(Call<ResponseContainerOneRow<PropertyOneResponse>> call, Throwable t) {

            }
        });

        delete = findViewById(R.id.delete);
        delete.setVisibility(View.INVISIBLE);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PropertyService service = ServiceGenerator.createService(PropertyService.class, UtilToken.getToken(ImagenActivity.this), TipoAutenticacion.JWT);
                Call<ResponseBody> call = service.delete(idObtenido);
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if(response.isSuccessful()){
                            Toast.makeText(ImagenActivity.this, "Borrado Satisfactoriamente", Toast.LENGTH_SHORT).show();
                            finish();

                        }else {
                            Toast.makeText(ImagenActivity.this, "No se ha podido borrar la foto", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.e("NetworkFailure", t.getMessage());
                        Toast.makeText(ImagenActivity.this, "Error de conexión", Toast.LENGTH_SHORT).show();

                    }
                });
            }
        });

    }
}
