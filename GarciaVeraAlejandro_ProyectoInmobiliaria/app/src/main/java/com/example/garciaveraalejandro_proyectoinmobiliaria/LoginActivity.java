package com.example.garciaveraalejandro_proyectoinmobiliaria;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.garciaveraalejandro_proyectoinmobiliaria.responses.LoginResponse;
import com.example.garciaveraalejandro_proyectoinmobiliaria.retrofit.UtilToken;
import com.example.garciaveraalejandro_proyectoinmobiliaria.retrofit.generator.ServiceGenerator;
import com.example.garciaveraalejandro_proyectoinmobiliaria.retrofit.services.LoginService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    EditText email, password;
    Button btn_login, btn_registro;
    TextView tv_registro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        btn_login = findViewById(R.id.btn_login);
        btn_registro = findViewById(R.id.btn_registro);
        tv_registro = findViewById(R.id.tv_registro);

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email_txt = email.getText().toString();
                String password_txt = password.getText().toString();

                // String credentials = email_txt + ":" + password_txt;

                //  final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);


                LoginService service = ServiceGenerator.createService(LoginService.class, email_txt, password_txt);
                Call<LoginResponse> call = service.doLogin();

                call.enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                        if (response.code() != 201) {
                            // error
                            Log.e("RequestError", response.message());
                            Toast.makeText(LoginActivity.this, "Error de petición", Toast.LENGTH_SHORT).show();
                        } else {
                            //UtilToken.getToken( LoginActivity.this);
                            UtilToken.setToken(LoginActivity.this, response.body().getToken());
                            Log.i("PRUEBA","Entra en evento click");
                            Intent i = new Intent(
                                    LoginActivity.this,
                                    PaginaPrincipal.class
                            );

                            startActivity(i);
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        Log.e("NetworkFailure", t.getMessage());
                        Toast.makeText(LoginActivity.this, "Error de conexión", Toast.LENGTH_SHORT).show();
                    }
                });


            }
        });

        tv_registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(
                        LoginActivity.this,
                        RegistroUser.class
                );

                startActivity(i);

            }
        });
    }
}