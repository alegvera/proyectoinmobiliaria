package com.example.garciaveraalejandro_proyectoinmobiliaria;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.example.garciaveraalejandro_proyectoinmobiliaria.model.Property;
import com.example.garciaveraalejandro_proyectoinmobiliaria.responses.PropertyResponse;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link PropertyResponse} and makes a call to the
 * specified {@link PropertyInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyPropertyRecyclerViewAdapter extends RecyclerView.Adapter<MyPropertyRecyclerViewAdapter.ViewHolder> {

    private final List<PropertyResponse> mValues;
    private final PropertyInteractionListener mListener;
    Context ctx;

    public MyPropertyRecyclerViewAdapter(Context ctx, List<PropertyResponse> items, PropertyInteractionListener listener) {
        mValues = items;
        mListener = listener;
        this.ctx = ctx;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_property, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.tvTitulo.setText(holder.mItem.getTitle());
        holder.tvPrice.setText(String.valueOf(holder.mItem.getPrice()));

        if(mValues.get(position).getPhotos() != null){
            Glide
                    .with(ctx)
                    .load(mValues.get(position).getPhotos().get(0))
                    .into(holder.ivUrl);
        }else{
            Glide
                    .with(ctx)
                    .load(R.drawable.casa)
                    .into(holder.ivUrl);
        }


        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onPropertyExpandClick(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView tvTitulo;
        public final TextView tvPrice;
        public final ImageView ivFav;
        public final ImageView ivUrl;
        public PropertyResponse mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            tvTitulo = view.findViewById(R.id.Titulo);
            tvPrice = view.findViewById(R.id.precios);
            ivFav = view.findViewById(R.id.imageViewFav);
            ivUrl = view.findViewById(R.id.imageViewUrlF);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + tvTitulo.getText() + "'";
        }
    }
}
