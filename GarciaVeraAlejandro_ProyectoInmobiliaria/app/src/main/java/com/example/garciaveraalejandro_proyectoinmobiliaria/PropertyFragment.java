package com.example.garciaveraalejandro_proyectoinmobiliaria;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.garciaveraalejandro_proyectoinmobiliaria.model.Property;
import com.example.garciaveraalejandro_proyectoinmobiliaria.model.ResponseContainer;
import com.example.garciaveraalejandro_proyectoinmobiliaria.responses.PropertyResponse;
import com.example.garciaveraalejandro_proyectoinmobiliaria.retrofit.UtilToken;
import com.example.garciaveraalejandro_proyectoinmobiliaria.retrofit.generator.ServiceGenerator;
import com.example.garciaveraalejandro_proyectoinmobiliaria.retrofit.generator.TipoAutenticacion;
import com.example.garciaveraalejandro_proyectoinmobiliaria.retrofit.services.PropertyService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link PropertyInteractionListener}
 * interface.
 */
public class PropertyFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private PropertyInteractionListener mListener;
    private MyPropertyRecyclerViewAdapter adapter;
    private Context ctx;
    private List<PropertyResponse> propertyList;
    private List<Property> propertyList2;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipe;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public PropertyFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static PropertyFragment newInstance(int columnCount) {
        PropertyFragment fragment = new PropertyFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_property_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view.findViewById(R.id.list);
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            propertyList = new ArrayList<>();
            this.getProperty(false);


        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.ctx = context;
        if (context instanceof PropertyInteractionListener) {
            mListener = (PropertyInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement PropertyInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void getProperty(final boolean update){
        PropertyService service = ServiceGenerator.createService(PropertyService.class);

        Call<ResponseContainer<PropertyResponse>> call = service.listProperty();

        call.enqueue(new Callback<ResponseContainer<PropertyResponse>>() {


            @Override
            public void onResponse(Call<ResponseContainer<PropertyResponse>> call, Response<ResponseContainer<PropertyResponse>> response) {
                if (response.isSuccessful()) {
                    // error
                    Log.e("RequestSuccessful", response.message());
                    propertyList = response.body().getRows();

                    adapter = new MyPropertyRecyclerViewAdapter(
                            ctx,
                            propertyList,
                            mListener
                    );
                    recyclerView.setAdapter(adapter);
                    //}

                } else {
                    Log.e("RequestError", response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseContainer<PropertyResponse>> call, Throwable t) {
                Log.e("NetworkFailure", t.getMessage());
                //Toast.makeText(FotoActivity.this, "Error de conexión", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        this.getProperty(true);

    }
}
