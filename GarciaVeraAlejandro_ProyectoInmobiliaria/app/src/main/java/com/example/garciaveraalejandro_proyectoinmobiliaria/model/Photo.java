package com.example.garciaveraalejandro_proyectoinmobiliaria.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Photo {
  @SerializedName("id")
  @Expose
  private String id;
  @SerializedName("propertyId")
  @Expose
  private String propertyId;
  @SerializedName("imgurLink")
  @Expose
  private String imgurLink;


  public Photo(){ }

  public Photo(String id, String propertyId, String imgurLink) {
    this.id = id;
    this.propertyId = propertyId;
    this.imgurLink = imgurLink;
  }

  public Photo(String propertyId, String imgurLink) {
    this.propertyId = propertyId;
    this.imgurLink = imgurLink;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getPropertyId() {
    return propertyId;
  }

  public void setPropertyId(String propertyId) {
    this.propertyId = propertyId;
  }

  public String getImgurLink() { return imgurLink; }

  public void setImgurLink(String imgurLink) { this.imgurLink = imgurLink; }
}
