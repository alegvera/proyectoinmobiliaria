package com.example.garciaveraalejandro_proyectoinmobiliaria.responses;

import com.example.garciaveraalejandro_proyectoinmobiliaria.model.Category;
import com.example.garciaveraalejandro_proyectoinmobiliaria.model.Photo;
import com.example.garciaveraalejandro_proyectoinmobiliaria.model.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class PropertyResponse {

  @SerializedName("id")
  @Expose
  private String id;
  @SerializedName("ownerId")
  @Expose
  private User ownerId;
  @SerializedName("title")
  @Expose
  private String title;
  @SerializedName("description")
  @Expose
  private String description;
  @SerializedName("price")
  @Expose
  private double price;
  @SerializedName("rooms")
  @Expose
  private int rooms;
  @SerializedName("size")
  @Expose
  private float size;
  @SerializedName("categoryId")
  @Expose
  private Category categoryId;
  @SerializedName("address")
  @Expose
  private String address;
  @SerializedName("zipcode")
  @Expose
  private String zipcode;
  @SerializedName("city")
  @Expose
  private String city;
  @SerializedName("province")
  @Expose
  private String province;
  @SerializedName("loc")
  @Expose
  private String loc;
  @SerializedName("photos")
  @Expose
  private List<String> photos;
  @SerializedName("listEsFav")
  @Expose
  private List<String> esFav;

  public PropertyResponse(){}
  //String []
  public PropertyResponse(String id, User ownerId, String title, String description, double price, int rooms, float size, Category categoryId, String address, String zipcode, String city, String province, String loc, List<String> photos, List<String> esFav) {
    this.id = id;
    this.ownerId = ownerId;
    this.title = title;
    this.description = description;
    this.price = price;
    this.rooms = rooms;
    this.size = size;
    this.categoryId = categoryId;
    this.address = address;
    this.zipcode = zipcode;
    this.city = city;
    this.province = province;
    this.loc = loc;
    this.photos = photos;
    this.esFav = esFav;
  }

  public PropertyResponse(User ownerId, String title, String description, double price, int rooms, float size, Category categoryId, String address, String zipcode, String city, String province, String loc, List<String> photos, List<String> esFav) {
    this.ownerId = ownerId;
    this.title = title;
    this.description = description;
    this.price = price;
    this.rooms = rooms;
    this.size = size;
    this.categoryId = categoryId;
    this.address = address;
    this.zipcode = zipcode;
    this.city = city;
    this.province = province;
    this.loc = loc;
    this.photos = photos;
    this.esFav = esFav;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public User getOwnerId() {
    return ownerId;
  }

  public void setOwnerId(User ownerId) {
    this.ownerId = ownerId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public int getRooms() {
    return rooms;
  }

  public void setRooms(int rooms) {
    this.rooms = rooms;
  }

  public float getSize() {
    return size;
  }

  public void setSize(float size) {
    this.size = size;
  }

  public Category getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(Category categoryId) {
    this.categoryId = categoryId;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getZipcode() {
    return zipcode;
  }

  public void setZipcode(String zipcode) {
    this.zipcode = zipcode;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getProvince() {
    return province;
  }

  public void setProvince(String province) {
    this.province = province;
  }

  public String getLoc() {
    return loc;
  }

  public void setLoc(String loc) {
    this.loc = loc;
  }

  public List<String> getPhotos() {
    return photos;
  }

  public void setPhotos(List<String> photos) {
    this.photos = photos;
  }

  public List<String> getEsFav() {
    return esFav;
  }

  public void setEsFav(List<String> esFav) {
    this.esFav = esFav;
  }
}
