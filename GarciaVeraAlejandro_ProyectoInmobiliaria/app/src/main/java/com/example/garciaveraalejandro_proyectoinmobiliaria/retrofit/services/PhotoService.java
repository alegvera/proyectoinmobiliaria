package com.example.garciaveraalejandro_proyectoinmobiliaria.retrofit.services;

import com.example.garciaveraalejandro_proyectoinmobiliaria.model.ResponseContainer;
import com.example.garciaveraalejandro_proyectoinmobiliaria.responses.PhotoResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface PhotoService {

    final String BASE_URL = "/photos";

    @GET(BASE_URL)
    Call<ResponseContainer<PhotoResponse>> getAll();

    @GET(BASE_URL + "/{id}")
    Call<PhotoResponse> getOne(@Path("id") String id);

    @DELETE(BASE_URL + "/{id}")
    Call<PhotoResponse> delete(@Path("id") String id);

}
