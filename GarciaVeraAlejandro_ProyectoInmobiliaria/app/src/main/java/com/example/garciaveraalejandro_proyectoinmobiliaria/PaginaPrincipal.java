package com.example.garciaveraalejandro_proyectoinmobiliaria;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.garciaveraalejandro_proyectoinmobiliaria.model.Property;
import com.example.garciaveraalejandro_proyectoinmobiliaria.model.ResponseContainer;
import com.example.garciaveraalejandro_proyectoinmobiliaria.responses.PropertyResponse;
import com.example.garciaveraalejandro_proyectoinmobiliaria.retrofit.UtilToken;
import com.example.garciaveraalejandro_proyectoinmobiliaria.retrofit.generator.ServiceGenerator;
import com.example.garciaveraalejandro_proyectoinmobiliaria.retrofit.services.PropertyService;

public class PaginaPrincipal extends AppCompatActivity implements PropertyInteractionListener {

    private TextView mTextMessage;
    private MenuItem menuItem;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.frameLayout, new PropertyFragment())
                            .commit();
                    return true;
                case R.id.navigation_dashboard:
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.frameLayout, new PropertyFragment()) //CAMBIAR
                            .commit();
                    return true;
                case R.id.navigation_favorites:
                    mTextMessage.setText(R.string.title_favorite);
                    return true;
                case R.id.navigation_profile:
                    mTextMessage.setText(R.string.title_profile);
                    return true;
            }
            return false;
        }
    };


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.session:
                if (UtilToken.getToken(PaginaPrincipal.this) == null) {
                    Intent i = new Intent(
                            PaginaPrincipal.this, LoginActivity.class
                    );
                    startActivity(i);
                } else {
                    LogOut();
                }
                return true;
            case R.id.buscar:
                Buscar();

        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_navigation, menu);
        menuItem = menu.findItem(R.id.session);
        if (UtilToken.getToken(PaginaPrincipal.this) != null) {
            menuItem.setTitle("Cerrar sesión");
            menuItem.setIcon(R.drawable.ic_exit_to_app_black_24dp);
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pagina_principal);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frameLayout, new PropertyFragment())
                .commit();

        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        Menu itemsMenu = navigation.getMenu();
        if (UtilToken.getToken(PaginaPrincipal.this) == null) {
            itemsMenu.findItem(R.id.navigation_profile).setVisible(false);
            itemsMenu.findItem(R.id.navigation_dashboard).setVisible(false);
            itemsMenu.findItem(R.id.navigation_favorites).setVisible(false);
        }

    }

    public void LogOut() {
        UtilToken.setToken(PaginaPrincipal.this, null);
        //SharedPreferences mPreferences = getSharedPreferences("login", MODE_PRIVATE);
        //SharedPreferences.Editor editor = mPreferences.edit();
        //editor.clear();
        //editor.commit();
        Intent intent = new Intent(PaginaPrincipal.this,
                PaginaPrincipal.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    public void onPropertyExpandClick(PropertyResponse property) {
        Intent i = new Intent(this, ImagenActivity.class);
        i.putExtra("PROPERTY_ID", property.getId());
        /*i.putExtra("PROPERTY_OWNERID", String.valueOf(property.getOwnerId().getName()));
        i.putExtra("PROPERTY_TITLE", property.getTitle());
        i.putExtra("PROPERTY_DESCRIPTION", property.getDescription());
        i.putExtra("PROPERTY_PRICE", String.valueOf(property.getPrice()));
        i.putExtra("PROPERTY_ROOMS", String.valueOf(property.getRooms()));
        i.putExtra("PROPERTY_SIZE", String.valueOf(property.getSize()));
        i.putExtra("PROPERTY_CATEGORYID", property.getCategoryId().getName());
        i.putExtra("PROPERTY_ADDRESS", property.getAddress());
        i.putExtra("PROPERTY_ZIPCODE", property.getZipcode());
        i.putExtra("PROPERTY_CITY", property.getCity());
        i.putExtra("PROPERTY_PROVINCE", property.getProvince());
        i.putExtra("PROPERTY_LOC", property.getLoc());*/
        startActivity(i);

    }

    @Override
    public void onClickDelete(String id) {
        PropertyService service = ServiceGenerator.createService(PropertyService.class);
        service.delete(id);
    }

    public void Buscar() {
    }

}
